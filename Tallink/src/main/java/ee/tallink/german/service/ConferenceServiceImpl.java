package ee.tallink.german.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.german.dao.ConferenceDao;
import ee.tallink.german.model.ConferenceModel;

@Service
public class ConferenceServiceImpl implements ConferenceService{
	
	@Autowired
	private ConferenceDao conferenceDao;
	
	public void setConferenceDao(ConferenceDao conferenceDao){
		this.conferenceDao = conferenceDao;
	}
	
	@Override
	public void addConference(ConferenceModel conference) {
		conferenceDao.addConference(conference);
		
	}

	@Override
	public ConferenceModel getConferenceById(int id) {
		return conferenceDao.getConferenceById(id);
		
	}

	@Override
	public List<ConferenceModel> getListConferences() {
		return conferenceDao.getListConferences();
	}

	@Override
	public int getCountEmptyPlace(int id) {
		return conferenceDao.getCountEmptyPalce(id);
	}

	@Override
	public void deleteConference(int id) {
		conferenceDao.deleteConference(id);
		
	}


}
