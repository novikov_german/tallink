package ee.tallink.german.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.german.dao.ParticipantInConferenceDao;
import ee.tallink.german.model.ParticipantInConferenceModel;

@Service
public class ParticipantInConferenceServiceImpl implements
		ParticipantInConferenceService {
	
	@Autowired
	private ParticipantInConferenceDao participantInConferenceDao;

	@Override
	public void removeParticipantFromConference(int participantId,
			int conferenceId) {
		participantInConferenceDao.removeParticipantFromConference(participantId, conferenceId);

	}

	@Override
	public void setParticipantInConference(
			ParticipantInConferenceModel participantInConferenceModel) {
		participantInConferenceDao.setParticipantInConference(participantInConferenceModel);

	}

}
