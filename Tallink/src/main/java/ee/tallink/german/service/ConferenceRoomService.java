package ee.tallink.german.service;

import java.util.Date;
import java.util.List;

import ee.tallink.german.model.ConferenceRoomModel;

public interface ConferenceRoomService {
	public void addConferenceRoom(ConferenceRoomModel conferenceRoom);
	public ConferenceRoomModel getConferenceRoomById(int id);
	public List<ConferenceRoomModel> getListConferenceRoom();
	public boolean checkIsFree(Date date, int conferenceRoomId);
}
