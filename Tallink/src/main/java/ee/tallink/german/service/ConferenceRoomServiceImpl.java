package ee.tallink.german.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.german.dao.ConferenceRoomDao;
import ee.tallink.german.model.ConferenceRoomModel;

@Service
public class ConferenceRoomServiceImpl implements ConferenceRoomService {
	@Autowired
	private ConferenceRoomDao conferenceRoomDao;
	
	public void setConferenceRoomDao(ConferenceRoomDao conferenceRoomDao){
		this.conferenceRoomDao = conferenceRoomDao;
	}
	@Override
	public void addConferenceRoom(ConferenceRoomModel conferenceRoom) {
		conferenceRoomDao.addConferenceRoom(conferenceRoom);

	}

	@Override
	public ConferenceRoomModel getConferenceRoomById(int id) {
		return conferenceRoomDao.getConferenceRoomById(id);
	}

	@Override
	public List<ConferenceRoomModel> getListConferenceRoom() {
		return conferenceRoomDao.getListConferenceRoom();
	}
	
	@Override
	public boolean checkIsFree(Date date, int conferenceRoomId) {
		return conferenceRoomDao.checkIsFree(date, conferenceRoomId);
		
	}
	
	

}
