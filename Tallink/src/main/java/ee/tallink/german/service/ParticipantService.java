package ee.tallink.german.service;

import java.util.Date;
import java.util.List;

import ee.tallink.german.model.ParticipantModel;

public interface ParticipantService {
	public void addParticipant(ParticipantModel participant);
	public void removeParticipant(int conferenceId, int participantId);
	public ParticipantModel getParticipantById(int id);
	public ParticipantModel getParticipantByNameAndBirthday(String participantName, Date birthday);
	public List<ParticipantModel> getAllParticipantInThisConference(int conferenceId);
}
