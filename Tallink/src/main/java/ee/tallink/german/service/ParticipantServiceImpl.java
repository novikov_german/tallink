package ee.tallink.german.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ee.tallink.german.dao.ParticipantDao;
import ee.tallink.german.model.ParticipantModel;

@Service
public class ParticipantServiceImpl implements ParticipantService {
	
	@Autowired
	private ParticipantDao participantDao;
	
	@Override
	public void addParticipant(ParticipantModel participant) {
		participantDao.addParticipant(participant);
	}

	@Override
	public void removeParticipant(int conferenceId, int participantId){
		participantDao.removeParticipant(conferenceId, participantId);
	}

	@Override
	public ParticipantModel getParticipantById(int id) {
		return participantDao.getParticipantById(id);
	}

	@Override
	public ParticipantModel getParticipantByNameAndBirthday(String participantName, Date birthday) {
		return participantDao.getParticipantByNameAndBirthday(participantName, birthday);
	}

	@Override
	public List<ParticipantModel> getAllParticipantInThisConference(
			int conferenceId) {
		return participantDao.getAllParticipantInThisConference(conferenceId);
	}

}
