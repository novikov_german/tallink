package ee.tallink.german.service;

import java.util.List;

import ee.tallink.german.model.ConferenceModel;

public interface ConferenceService {
	public void addConference(ConferenceModel conference);
	public ConferenceModel getConferenceById(int id);
	public List<ConferenceModel> getListConferences();
	public int getCountEmptyPlace(int id);
	public void deleteConference(int id);
}
