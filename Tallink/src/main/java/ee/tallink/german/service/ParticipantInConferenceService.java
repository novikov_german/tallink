package ee.tallink.german.service;

import ee.tallink.german.model.ParticipantInConferenceModel;

public interface ParticipantInConferenceService {
	public void removeParticipantFromConference(int participantId, int conferenceId);
	public void setParticipantInConference(ParticipantInConferenceModel participantInConferenceModel);
}
