package ee.tallink.german.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ee.tallink.german.model.ParticipantInConferenceModel;
import ee.tallink.german.model.ParticipantModel;
import ee.tallink.german.service.ConferenceService;
import ee.tallink.german.service.ParticipantInConferenceService;
import ee.tallink.german.service.ParticipantInConferenceServiceImpl;
import ee.tallink.german.service.ParticipantService;


@Controller
@RequestMapping("/registration")
public class RegistrationController {
	@Autowired
	private ParticipantService participantService; 
	
	@Autowired
	private ConferenceService conferenceService;
	
	@Autowired
	private ParticipantInConferenceService participantInConferenceService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayRegistrationForm(@RequestParam("id") int conferenceId, Model model){
		model.addAttribute("participant", new ParticipantModel());
		model.addAttribute("conference", conferenceService.getConferenceById(conferenceId));
		model.addAttribute("command", ""); 
		return "registration-form";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String register(@RequestParam("id") int conferenceId, @ModelAttribute("participant") ParticipantModel participant,
			BindingResult errors, HttpServletRequest request, Model model){
		if(conferenceService.getCountEmptyPlace(conferenceId) == 0){
			model.addAttribute("message", "Sorry, no empty place");
			
		}else{
			ParticipantModel foudedParticipant = participantService.getParticipantByNameAndBirthday(participant.getFullName(), 
					participant.getBirthday());
			if(foudedParticipant != null){
				registrationParticipantInConference(conferenceId, foudedParticipant);
			}else{
				participantService.addParticipant(participant);
				ParticipantModel addedParticipant = participantService.getParticipantByNameAndBirthday(participant.getFullName(), 
						participant.getBirthday());
				registrationParticipantInConference(conferenceId, addedParticipant);
			}
			model.addAttribute("message", "You successfully registered!");
		}
	return "redirect:" + "/home";
	}
	
	private void registrationParticipantInConference(int conferenceId, ParticipantModel participant){
		ParticipantInConferenceModel participantInConferenceModel = new ParticipantInConferenceModel();
		participantInConferenceModel.setConferenceId(conferenceId);
		participantInConferenceModel.setParticipantId(participant.getId());
		participantInConferenceService.setParticipantInConference(participantInConferenceModel);
	}
}
