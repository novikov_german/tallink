package ee.tallink.german.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ee.tallink.german.service.ConferenceRoomService;

@Controller
@RequestMapping("/rooms")
public class RoomsController {
	
	@Autowired
	private ConferenceRoomService conferenceRoomService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getConferences(Model model){
		model.addAttribute("listConferenceRoom", conferenceRoomService.getListConferenceRoom());
		return "rooms";
	}

}
