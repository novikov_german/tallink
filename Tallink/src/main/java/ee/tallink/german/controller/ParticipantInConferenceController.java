package ee.tallink.german.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ee.tallink.german.model.ParticipantModel;
import ee.tallink.german.service.ConferenceService;
import ee.tallink.german.service.ParticipantService;
import ee.tallink.german.service.ParticipantServiceImpl;

@Controller
@RequestMapping("/participant")
public class ParticipantInConferenceController {
	@Autowired
	private ParticipantService participantService = new ParticipantServiceImpl();
	
	@Autowired
	private ConferenceService conferenceService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getConferences(@RequestParam("conferenceId") int conferenceId , Model model){
		model.addAttribute("conference", conferenceService.getConferenceById(conferenceId));
		model.addAttribute("listParticipant", participantService.getAllParticipantInThisConference(conferenceId));
		return "participant";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.GET)
	public String deleteConference(@RequestParam("participantId") int participantId,@RequestParam("conferenceId") int conferenceId){
		participantService.removeParticipant(conferenceId, participantId);
		return "redirect:" + "/home";
	}

}
