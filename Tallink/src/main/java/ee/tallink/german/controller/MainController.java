package ee.tallink.german.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ee.tallink.german.model.ConferenceInformationModel;
import ee.tallink.german.model.ConferenceModel;
import ee.tallink.german.model.ConferenceRoomModel;
import ee.tallink.german.model.ParticipantModel;
import ee.tallink.german.service.ConferenceRoomService;
import ee.tallink.german.service.ConferenceService;

@Controller
@RequestMapping("/home")
public class MainController {
	
	@Autowired
	private ConferenceService conferenceService;
	
	@Autowired
	private ConferenceRoomService conferenceRoomService;
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String getConferences(Model model){
		model.addAttribute("listConferences", conferenceService.getListConferences());
		return "index";
	}
	
	@RequestMapping(value = "conference-information", method = RequestMethod.GET)
    @ResponseBody
	public String getConferenceInformation(@RequestParam("id") int id,Model model){
		ConferenceInformationModel conferenceInformation = new ConferenceInformationModel();
		ConferenceModel conference= conferenceService.getConferenceById(id);
		ConferenceRoomModel room = conferenceRoomService.getConferenceRoomById(conference.getConferenceRoom());
		
		int emptySeats = conferenceService.getCountEmptyPlace(id);
		emptySeats = (emptySeats == -1)? room.getMaxSeats() : emptySeats;	
		conferenceInformation.setConference(conference);
		conferenceInformation.setConferenceRoom(room);
		conferenceInformation.setEmptySeats(emptySeats);
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writeValueAsString(conferenceInformation);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	

}