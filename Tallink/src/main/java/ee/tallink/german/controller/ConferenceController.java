package ee.tallink.german.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ee.tallink.german.model.ConferenceModel;
import ee.tallink.german.model.ConferenceRoomModel;
import ee.tallink.german.model.ParticipantModel;
import ee.tallink.german.service.ConferenceRoomService;
import ee.tallink.german.service.ConferenceRoomServiceImpl;
import ee.tallink.german.service.ConferenceService;

@Controller
@RequestMapping("/conference")
public class ConferenceController {
	@Autowired
	private ConferenceService conferenceService;
	
	@Autowired
	private ConferenceRoomService conferenceRoomService;

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public String getConferenceForm(Model model){
		model.addAttribute("conference", new ConferenceModel());
		List<ConferenceRoomModel> rooms = conferenceRoomService.getListConferenceRoom();
		model.addAttribute("rooms", rooms);
		return "create-conference";
	}
	
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String createNewConference(@ModelAttribute("conference") ConferenceModel conference,
			BindingResult errors, HttpServletRequest request){
		if(conferenceRoomService.checkIsFree(conference.getDateTime(), conference.getConferenceRoom())){
			conferenceService.addConference(conference);
			return "redirect:" + "/home";
		}else{
			return "create_conference";
		}
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.GET)
	public String deleteConference(@RequestParam("conferenceId") int conferenceId){
		conferenceService.deleteConference(conferenceId);
		return "redirect:" + "/home";
	}
	
}
