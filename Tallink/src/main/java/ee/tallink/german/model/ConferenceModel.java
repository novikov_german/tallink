package ee.tallink.german.model;

import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ConferenceModel {
	private int id;
	private String conferenceName;	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	private Date dateTime;
	private int conferenceRoom;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConferenceName() {
		return conferenceName;
	}

	public void setConferenceName(String conferenceName) {
		this.conferenceName = conferenceName;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public int getConferenceRoom() {
		return conferenceRoom;
	}

	public void setConferenceRoom(int roomId) {
		this.conferenceRoom = roomId;
	}
	
	
	
}
