package ee.tallink.german.model;

public class ParticipantInConferenceModel {
	
	private int conferenceId;
	private int particiantId;

	public int getConferenceId() {
		return conferenceId;
	}

	public void setConferenceId(int conferenceId) {
		this.conferenceId = conferenceId;
	}

	public int getParticiantId() {
		return particiantId;
	}

	public void setParticipantId(int particiantId) {
		this.particiantId = particiantId;
	}
	
	

}
