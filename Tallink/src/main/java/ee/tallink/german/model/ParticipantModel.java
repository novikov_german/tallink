package ee.tallink.german.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ParticipantModel {
	private static final String BIRTHDAY_DATE_FORMAT = "yyyy-MM-dd";
	private int id;
	private String fullName;
	@DateTimeFormat(pattern = BIRTHDAY_DATE_FORMAT)
	private Date birthday;

	public ParticipantModel(){
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public void setBirthday(String birthday) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(BIRTHDAY_DATE_FORMAT);
		this.birthday = formatter.parse(birthday);
	}
}
