package ee.tallink.german.model;


public class ConferenceInformationModel {
	private ConferenceModel conference;
	private ConferenceRoomModel conferenceRoom;
	private int emptySeats;
	
	public ConferenceModel getConference() {
		return conference;
	}
	public void setConference(ConferenceModel conference) {
		this.conference = conference;
	}
	public ConferenceRoomModel getConferenceRoom() {
		return conferenceRoom;
	}
	public void setConferenceRoom(ConferenceRoomModel conferenceRoom) {
		this.conferenceRoom = conferenceRoom;
	}
	public int getEmptySeats() {
		return emptySeats;
	}
	public void setEmptySeats(int emptySeats) {
		this.emptySeats = emptySeats;
	}
}
