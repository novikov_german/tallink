package ee.tallink.german.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ee.tallink.german.model.ConferenceModel;
import ee.tallink.german.model.ConferenceRoomModel;
@Repository
public class ConferenceRoomDaoImpl implements ConferenceRoomDao {
	
	private JdbcTemplate jdbcTemplate;
	private static final Logger logger = LoggerFactory.getLogger(ConferenceDaoImpl.class);
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void addConferenceRoom(ConferenceRoomModel conferenceRoom) {
		String sql = "insert into Conference_room (name, location, max_seats) VALUES (?, ?)";
		jdbcTemplate.update(sql, new Object[]{conferenceRoom.getName(), 
				conferenceRoom.getLocation(),conferenceRoom.getMaxSeats()});
		logger.info("Conference room successfuly created! " + conferenceRoom);
		
	}

	@Override
	public ConferenceRoomModel getConferenceRoomById(int id) {
		String sql = "select * from Conference_room where id = ?";

		ConferenceRoomModel conferenceRoom = jdbcTemplate.queryForObject(sql, new Object[]{id}, new ConferenceRoomRowMapper());
		if(conferenceRoom != null){
			logger.info("Conference room successfuly loaded! " + conferenceRoom);
			return conferenceRoom;
		}
		else{
			logger.info("Conference room not found! " + id);
		}
		
		return null;
	}
	
	@Override
	public List<ConferenceRoomModel> getListConferenceRoom() {
		String sql = "select * from Conference_room";
		return jdbcTemplate.query(sql, new ConferenceRoomRowMapper());
	}
	
	@Override
	public boolean checkIsFree(Date date, int conferenceRoomId) {
		String sql = "select count(*) from Conference where conference_room = ? and date_time = ?";
		int rooms = jdbcTemplate.queryForObject(sql, Integer.class, conferenceRoomId, date);
		if(rooms == 0){
			return true;
		}else{
			return false;
		}
	}
	
	private static final class ConferenceRoomRowMapper implements RowMapper<ConferenceRoomModel>{

		@Override
		public ConferenceRoomModel mapRow(ResultSet rs, int rowNumb)
				throws SQLException {
			ConferenceRoomModel conferenceRoom = new ConferenceRoomModel();
			conferenceRoom.setId(rs.getInt("id"));
			conferenceRoom.setName(rs.getString("name"));
			conferenceRoom.setLocation(rs.getString("location"));
			conferenceRoom.setMaxSeats(rs.getInt("max_seats"));
			return conferenceRoom;
		}
		
	}

	

}
