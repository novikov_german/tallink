package ee.tallink.german.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import ee.tallink.german.model.ConferenceModel;

@Repository
//@Qualifier("conferenceDaoImpl")
//@Component("conferenceDaoImpl")
public class ConferenceDaoImpl implements ConferenceDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(ConferenceDaoImpl.class);
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	
	@Override
	public void addConference(ConferenceModel conference) {
		String sql = "insert into Conference (conference_name , date_time, conference_room) VALUES (?, datetime(? / 1000, 'unixepoch', 'localtime'), ?)";
		jdbcTemplate.update(sql, new Object[]{conference.getConferenceName(), conference.getDateTime().getTime(), conference.getConferenceRoom() });
		logger.info("Conference successfuly created! " + conference);
	}

	@Override
	public ConferenceModel getConferenceById(int id) {
		String sql = "select * from Conference where id =?";
		
		
		ConferenceModel conference = jdbcTemplate.queryForObject(sql, new Object[]{id}, new ConferenceRowMapper());
		if(conference != null){
			logger.info("Conference successfuly loaded! " + conference);
			return conference;
		}
		else{
			logger.info("Conference not found! " + id);
			return null;
		}
	}

	@Override
	public List<ConferenceModel> getListConferences() {
		String sql = "select id, conference_name, date_time, conference_room from Conference";
		return jdbcTemplate.query(sql, new ConferenceRowMapper());
	}

	@Override
	public int getCountEmptyPalce(int id) {
		String sql = "select ifnull((room.max_seats - count(*)), -1) from Participant_in_conference as participant "
		+"join Conference as conference on conference.id = participant.conference_id "
				+"join Conference_room as room on room.id = conference.conference_room "
				+"where conference.id = ?";
		
		return jdbcTemplate.queryForObject(sql, Integer.class, id);
	}

	private static final class ConferenceRowMapper implements RowMapper<ConferenceModel>{

		@Override
		public ConferenceModel mapRow(ResultSet rs, int rowNumb)
				throws SQLException {
			ConferenceModel conference = new ConferenceModel();
			conference.setId(rs.getInt("id"));
			conference.setConferenceName(rs.getString("conference_name"));
			try {
				conference.setDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("date_time")));
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
			conference.setConferenceRoom(rs.getInt("conference_room"));
			return conference;
		}
		
	}

	@Override
	public void deleteConference(int id) {
		String sql = "DELETE from Conference where id =?";
		jdbcTemplate.update(sql, new Object[]{id});
		logger.info("Conference successfuly removed! " + id);
	}

	

}
