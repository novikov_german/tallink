package ee.tallink.german.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import ee.tallink.german.model.ParticipantModel;
@Repository
public class ParticipantDaoImpl implements ParticipantDao {
	
	private JdbcTemplate jdbcTemplate;
	private static final Logger logger = LoggerFactory.getLogger(ConferenceDaoImpl.class);
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void addParticipant(ParticipantModel participant) {
		String sql = "insert into Participant ( full_name, birthday) VALUES (?, date(? / 1000, 'unixepoch', 'localtime'))";
		jdbcTemplate.update(sql, new Object[]{participant.getFullName(), participant.getBirthday().getTime() });
		logger.info("Participant successfuly added! " + participant);
		
	}
	

	@Override
	public void removeParticipant(int conferenceId, int participantId) {
		String sql = "delete from Participant_in_conference where conference_id =? and participant_id =?";
		jdbcTemplate.update(sql,new Object[]{conferenceId, participantId} );
		logger.info("Participant successfuly removed! " + participantId);
		
	}

	@Override
	public ParticipantModel getParticipantById(int id) {
		String sql = "select * from Participant where id =?";

		ParticipantModel participant = jdbcTemplate.queryForObject(sql, new Object[]{id}, new ParticipantRowMapper());
		if(participant != null){
			logger.info("Participant successfuly loaded! " + participant);
			return participant;
		}
		else{
			logger.info("Participant not found! " + id);
			return null;
		}
	}
	@Override
	public ParticipantModel getParticipantByNameAndBirthday(String participantName, Date birthday) {
		String sql = "select * from Participant where full_name =? and birthday = date(? / 1000, 'unixepoch', 'localtime')";
		List<ParticipantModel> participants = jdbcTemplate.query(sql, new Object[]{participantName, birthday }, new ParticipantRowMapper());
		if(participants != null){
			logger.info("Participant successfuly loaded! " + participants);
			return participants.isEmpty() ? null : participants.get(0);
		}
		else{
			logger.info("Participant not found! " + participantName);
			return null;
		}
	}
	
	@Override
	public List<ParticipantModel> getAllParticipantInThisConference(
			int conferenceId) {
		String sql = "select participant.id, participant.full_name, participant.birthday " + 
				"from Participant AS participant " +
				"inner join Participant_in_conference ON Participant_in_conference.participant_id = participant.id " +
				"where Participant_in_conference.conference_id = ?";	
		return jdbcTemplate.query(sql, new ParticipantRowMapper(), conferenceId);
	}
	
	
	private static final class ParticipantRowMapper implements RowMapper<ParticipantModel>{
		@Override
		public ParticipantModel mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			ParticipantModel participant = new ParticipantModel();
			participant.setId(rs.getInt("id"));
			participant.setFullName(rs.getString("full_name"));
			try {
				participant.setBirthday(rs.getString("birthday"));
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
			return participant;
		}
		
	}

	





	

}
