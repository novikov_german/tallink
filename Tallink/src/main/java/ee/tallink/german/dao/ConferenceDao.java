package ee.tallink.german.dao;

import java.util.List;

import ee.tallink.german.model.ConferenceModel;

public interface ConferenceDao {
	public void addConference(ConferenceModel conference);
	public ConferenceModel getConferenceById(int id);
	public List<ConferenceModel> getListConferences();
	public void deleteConference(int id);
	public int getCountEmptyPalce(int id);
}
