package ee.tallink.german.dao;

import java.util.List;

import ee.tallink.german.model.ParticipantInConferenceModel;

public interface ParticipantInConferenceDao {
	public void removeParticipantFromConference(int participantId, int conferenceId);
	public void setParticipantInConference(ParticipantInConferenceModel participantInConferenceModel);
}
