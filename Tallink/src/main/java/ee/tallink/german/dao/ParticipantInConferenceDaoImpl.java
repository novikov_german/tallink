package ee.tallink.german.dao;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import ee.tallink.german.model.ParticipantInConferenceModel;
@Repository
public class ParticipantInConferenceDaoImpl implements ParticipantInConferenceDao{
	
	private JdbcTemplate jdbcTemplate;
	private static final Logger logger = LoggerFactory.getLogger(ConferenceDaoImpl.class);
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void removeParticipantFromConference(int participantId, int conferenceId) {
		String sql = "delete ParticipantFromConference where participant_id = :participantId "
				+ "and conferenceId = :conferenceId";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("participant_id", participantId);
		params.addValue("conference_id", conferenceId);
		jdbcTemplate.update(sql, params);
		
		logger.info("Participant successfuly removed from conference! ");
	}

	@Override
	public void setParticipantInConference(
			ParticipantInConferenceModel participantInConferenceModel) {
		String sql = "insert into Participant_in_conference (conference_id , participant_id) VALUES (?, ?)";
		jdbcTemplate.update(sql, new Object[]{participantInConferenceModel.getConferenceId(),
				participantInConferenceModel.getParticiantId() });
		logger.info("Participant successfuly added to conference! " + participantInConferenceModel);
		
	}
	
	

}
