package ee.tallink.german.dao;

import java.util.Date;
import java.util.List;

import ee.tallink.german.model.ConferenceRoomModel;


public interface ConferenceRoomDao {
	public void addConferenceRoom(ConferenceRoomModel conferenceRoom);
	public ConferenceRoomModel getConferenceRoomById(int id);
	public List<ConferenceRoomModel> getListConferenceRoom();
	public boolean checkIsFree(Date date, int confernceRoomId);
}
