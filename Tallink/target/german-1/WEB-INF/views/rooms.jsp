<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<link
	href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />"
	rel="stylesheet">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>German Novikov "Tallink"</title>
</head>
<body>
	<div class="page-header text-center">
		<h1>
			German Novikov <small>Test work</small>
		</h1>
	</div>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="collapse navbar-collapse ">
			<ul class="nav navbar-nav">
				<li><a href="/german/home">List of conferences</a></li>
				<li><a href="/german/conference/create">Create conference</a></li>
				<li><a href="/german/rooms">Rooms</a></li>
		</div>
	</div>
	</nav>

	<div class="col-md-3"></div>
	<div class="col-md-6">
		<table class="table table-hover text-center">
			<tr>
				<th class="text-center">Id</th>
				<th class="text-center">Room name</th>
				<th class="text-center">Location</th>
				<th class="text-center">Max seats</th>
				<th></th>
				<th></th>
			</tr>
			<tbody>
				<c:forEach items="${listConferenceRoom}" var="conferenceRoom">
					<tr>
						<td>${conferenceRoom.id}</td>
						<td>${conferenceRoom.name}</td>
						<td>${conferenceRoom.location}</a></td>
						<td>${conferenceRoom.maxSeats}</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div></div>

	</div>
	<div class="col-md-3"></div>
</body>
</html>