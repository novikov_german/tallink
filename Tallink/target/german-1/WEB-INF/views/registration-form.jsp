<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ee.tallink.german.model.ConferenceModel"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%><html>
<head>
<link
	href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />"
	rel="stylesheet">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>German Novikov "Tallink"</title>
</head>
<body>
	<div class="page-header text-center">
		<h1>
			German Novikov <small>Test work</small>
		</h1>
	</div>
	<nav class="navbar navbar-default">
	<div class="container-fluid row">
		<div class="col-md-3"></div>
		<div class="collapse navbar-collapse "
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/german/home">List of conferences</a></li>
				<li><a href="/german/conference/create">Create conference</a></li>
				<li><a href="/german/rooms">Rooms</a></li>
		</div>
	</div>
	</nav>

	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<h3 class="text-center">Register to ${conference.conferenceName}
				conference</h3>
			<form:form method="POST" modelAttribute="participant">
				<div class="form-group">
					<form:label path="fullName">Full name</form:label>
					<form:input type="text" class="form-control" path="fullName"
						placeholder="Full name" />
				</div>
				<div class="form-group">
					<form:label path="birthday">Date of Birth</form:label>
					<form:input type="text" class="form-control" path="birthday"
						placeholder="yyyy-MM-dd" />
				</div>
				<button type="submit" class="btn btn-default">Register</button>
			</form:form>
		</div>
		<div class="col-md-3"></div>
</body>
</html>