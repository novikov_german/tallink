<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ee.tallink.german.model.ConferenceModel"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<link
	href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/jquery/jquery-3.2.0.js" />"></script>
<script src="<c:url value="/resources/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>

<script>
function getMoreInformation(conferenceId){
	$.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/german/home/conference-information?id=" + conferenceId,
        timeout: 600000,
        success: function (data) {
        	if(data != null) {
				setConferenceInformationData(jQuery.parseJSON(data));
        	} else {
        		alert(error);
        	}
        },
        error: function (e) {
			alert(error);
        }
});
}

function setConferenceInformationData(conferenceInformation) {
	$("#conferenceName").text(conferenceInformation.conference.conferenceName);
	$("#conferenceId").text(conferenceInformation.conference.id);
	$("#conferenceNameTable").text(conferenceInformation.conference.conferenceName);
	$("#conferenceRoomName").text(conferenceInformation.conferenceRoom.name);
	var date = new Date(conferenceInformation.conference.dateTime);
 	var curr_month = date.getMonth() + 1;
	var stringDateTime = date.getFullYear() + "-" + curr_month + "-" +
		date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
	$("#conferenceDateTime").text(stringDateTime);
// 	new Date(conferenceInformation.conference.dateTime).toString('yyyy-MM-dd'));
	$("#conferenceMaxSeats").text(conferenceInformation.conferenceRoom.maxSeats);
	$("#conferenceEmtySesats").text(conferenceInformation.emptySeats);
	$("#registrationLink").attr("href", "/german/registration?id=" + conferenceInformation.conference.id);
	$("#participantLink").attr("href", "/german/participant?conferenceId=" + conferenceInformation.conference.id);
	$("#deleteLink").attr("href", "/german/conference/delete?conferenceId=" + conferenceInformation.conference.id);
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>German Novikov "Tallink"</title>
</head>
<body>
	<div class="page-header text-center">
		<h1>
			German Novikov <small>Test work</small>
		</h1>
	</div>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="collapse navbar-collapse ">
			<ul class="nav navbar-nav">
				<li><a href="/german/home">List of conferences</a></li>
				<li><a href="/german/conference/create">Create conference</a></li>
				<li><a href="/german/rooms">Rooms</a></li>
		</div>
	</div>
	</nav>
	<c:if test="${message != null }">
		<div class="alert alert-success" role="alert">${message}</div>
	</c:if>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<table class="table table-hover text-center">
				<tr>
					<th class="text-center">Id</th>
					<th class="text-center">Conference name</th>
					<th></th>
					<th></th>
				</tr>
				<tbody>
					<c:forEach items="${listConferences}" var="conference">
						<tr>
							<td>${conference.id}</td>
							<td>${conference.conferenceName}</td>
							<td><a
								href="#" onclick="getMoreInformation(${conference.id})"
								data-toggle="modal" data-target="#myModal">More information</a></td>
							<td><a href="/german/registration?id=${conference.id}">Registration</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div></div>

		</div>
		<div class="col-md-3"></div>
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog" tabindex="-1">
			<div class="modal-dialog">

<!-- 				Modal content -->
				<div class="modal-content">
					<div class="modal-header text-center">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><span id="conferenceName"/></h4>
					</div>
					<div class="modal-body">
						<table width=100% class="text-center">
							<tr>
								<td>Id:</td>
								<td><span id="conferenceId"/></td>
							</tr>
							<tr>
								<td>Name:</td>
								<td><span id="conferenceNameTable"/></td>
							</tr>
							<tr>
								<td>Room:</td>
								<td><span id="conferenceRoomName"/></td>
							</tr>
							<tr>
								<td>Date and time:</td>
								<td><span id="conferenceDateTime"/>

								</td>
							</tr>
							<tr>
								<td>Max seats:</td>
								<td><span id="conferenceMaxSeats"/></td>
							</tr>
							<tr>
								<td>Empty seats:</td>
								<td><span id="conferenceEmtySesats"/></td>
							</tr>
							<tr>
								<td colspan="2">
									<a id="registrationLink" class="btn btn-primary"
										href="#"
										role="button">Registration</a> 
									<a id="participantLink" class="btn btn-primary"
										href="#"
										role="button">Show all participants</a> 
									<a id="deleteLink" class="btn btn-primary"
										href="#"
										role="button">Delete conference</a>
									</td>
							<tr>
						</table>
					
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
</body>
</html>