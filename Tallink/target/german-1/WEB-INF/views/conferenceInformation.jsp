<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="ee.tallink.german.model.ConferenceModel"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<link
	href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/jquery/jquery-3.2.0.js" />"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${conferenceInformation != null}">
		<table width=100% class="text-center">
			<tr>
				<td>Id:</td>
				<td>${conferenceInformation.id}</td>
			</tr>
			<tr>
				<td>Name:</td>
				<td>${conferenceInformation.conferenceName}</td>
			</tr>
			<tr>
				<td>Room:</td>
				<td>${conferenceRoomName}</td>
			</tr>
			<tr>
				<td>Date and time:</td>
				<td>
					<%
							ConferenceModel conference = (ConferenceModel) request.getAttribute("conferenceInformation");
							SimpleDateFormat  format = new SimpleDateFormat("dd-MM-yyyy  HH:mm");
							out.println(format.format(conference.getDateTime()));
							%>
				</td>
			</tr>
			<tr>
				<td>Max seats:</td>
				<td>${conferenceMaxSeats}</td>
			</tr>
			<tr>
				<td>Empty seats:</td>
				<td>${conferenceEmtySesats}</td>
			</tr>
			<tr>
				<td colspan="2">
					<a class="btn btn-primary"
						href="/german/registration?id=${conferenceInformation.id}"
						role="button">Registration</a> 
					<a class="btn btn-primary"
						href="/german/participant?conferenceId=${conferenceInformation.id}"
						role="button">Show all participant</a> 
					<a class="btn btn-primary"
						href="/german/conference/delete?conferenceId=${conferenceInformation.id}"
						role="button">Delete conference</a>
					</td>
			<tr>
		</table>
	</c:if>
</body>
</html>